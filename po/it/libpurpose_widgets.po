# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the purpose package.
# Vincenzo Reale <smart2128@baslug.org>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: purpose\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-29 02:10+0200\n"
"PO-Revision-Date: 2020-01-13 11:36+0100\n"
"Last-Translator: Vincenzo Reale <smart2128@baslug.org>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.12.1\n"

#: JobDialog.qml:39
#, kde-format
msgid "Configuration cancelled"
msgstr "Configurazione annullata"
