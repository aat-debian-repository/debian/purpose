# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2015, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-01 02:12+0200\n"
"PO-Revision-Date: 2020-08-29 10:30+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: AlternativesView.qml:31
#, kde-format
msgid "Use"
msgstr "Use"

#: JobView.qml:99
#, kde-format
msgid "Run"
msgstr "Run"

#: JobView.qml:105
#, kde-format
msgid "Back"
msgstr "Tillbaka"
