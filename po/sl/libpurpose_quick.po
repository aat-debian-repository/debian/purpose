# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the purpose package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: purpose\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-01 02:12+0200\n"
"PO-Revision-Date: 2020-05-19 11:00+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#: AlternativesView.qml:31
#, kde-format
msgid "Use"
msgstr "Uporabi"

#: JobView.qml:99
#, kde-format
msgid "Run"
msgstr "Poženi"

#: JobView.qml:105
#, kde-format
msgid "Back"
msgstr "Nazaj"
