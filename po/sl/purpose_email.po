# Slovenian translation of purpose
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the purpose package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: purpose\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-03 02:07+0200\n"
"PO-Revision-Date: 2020-01-23 15:58+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 2.2.4\n"

#: emailplugin.cpp:91 emailplugin.cpp:124
#, kde-format
msgid "Failed to launch email client"
msgstr "Zagon e-poštnega odjemalca ni uspel"
