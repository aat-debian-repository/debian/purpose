# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the purpose package.
# Wantoyo <wantoyek@gmail.com>, 2019, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: purpose\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-03 02:07+0200\n"
"PO-Revision-Date: 2019-08-28 18:55+0700\n"
"Last-Translator: Wantoyo <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.12.1\n"

#: bluetoothplugin_config.qml:23
#, kde-format
msgid "Choose a device to send to:"
msgstr "Pilihlah sebuah perangkat untuk dikirimi:"

#: bluetoothplugin_config.qml:56
#, kde-format
msgid "No devices found"
msgstr "Tidak ada perangkat yang ditemukan"
