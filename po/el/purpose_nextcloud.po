# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the purpose package.
#
# Stelios <sstavra@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: purpose\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-08-03 02:07+0200\n"
"PO-Revision-Date: 2020-10-21 12:38+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: nextcloudplugin_config.qml:38
#, kde-format
msgid "Account:"
msgstr "Λογαριασμός:"

#: nextcloudplugin_config.qml:59
#, kde-format
msgid "Upload to folder:"
msgstr "Αποστολή στον φάκελο:"
